const mongoose = require('mongoose')

const usersSchema = new mongoose.Schema({
  name: String,
  gender: String
})
module.exports = mongoose.model('User', usersSchema)
